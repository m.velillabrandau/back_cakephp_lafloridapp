<?php
namespace App\Controller;

use App\Controller\AppController;
use Phpml\Clustering\KMeans;

/**
 * Incidents Controller
 *
 * @property \App\Model\Table\IncidentsTable $Incidents
 *
 * @method \App\Model\Entity\Incident[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class IncidentsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $green = 'https://www.dropbox.com/s/22qd59ll3ztq96x/marker-green.png?dl=1';
        $red = 'https://www.dropbox.com/s/4szn03oymi1fq5s/marker-red.png?dl=1';
        $blue = 'https://www.dropbox.com/s/o4int3sjwoa680c/marker-blue.png?dl=1';
        $yellow = 'https://www.dropbox.com/s/jik4mvtc3ijeogj/marker-yellow.png?dl=1';
        $orange = 'https://www.dropbox.com/s/sbawj09z5do82co/marker-orange.png?dl=1';


        $incidents = $this->Incidents->find('all')
        ->where(['active' => 1])
        ->toArray();

        foreach ($incidents as $key => $incidence) {
            switch ($incidence['tag']) {
                case 'Transito':
                    $incidents[$key]['marker'] = $orange; 
                    break;
                case 'Parque':
                    $incidents[$key]['marker'] = $green; 
                    break;
                case 'Electrica':
                    $incidents[$key]['marker'] = $yellow;
                    break;
                case 'Agua':
                    $incidents[$key]['marker'] = $blue;
                    break;
            }
        }


        $this->response = $this->response->withType('application/json')->withStringBody(json_encode($incidents));

        return $this->response;



        
    }

    /**
     * Filtros
     *
     * @return \Cake\Http\Response|void
     */
    public function indexFilter()
    {
        $green = 'https://www.dropbox.com/s/22qd59ll3ztq96x/marker-green.png?dl=1';
        $red = 'https://www.dropbox.com/s/4szn03oymi1fq5s/marker-red.png?dl=1';
        $blue = 'https://www.dropbox.com/s/o4int3sjwoa680c/marker-blue.png?dl=1';
        $yellow = 'https://www.dropbox.com/s/jik4mvtc3ijeogj/marker-yellow.png?dl=1';
        $orange = 'https://www.dropbox.com/s/sbawj09z5do82co/marker-orange.png?dl=1';


        $filters = $this->request->getData();

        if(sizeof($filters) == 0){
            $incidents = $this->Incidents->find('all')
            ->where(['active' => 1])
            ->toArray();
        }else{
            
            $transito = $this->request->getData('transito');
            $agua = $this->request->getData('agua');
            $parque = $this->request->getData('parque');
            $electrico = $this->request->getData('electrico');
 
            $incidents = $this->Incidents->find()->where(function ($exp, $incidents) use ($filters){
                // Use add(incidents) to add multiple conditions for the same field.
                $author = $exp->or_([]);
                foreach ($filters as $keyFilter => $filter) {
                    // pr($filter);
                    $author->add(['tag' => $filter ]);
                }
                // $author->add(['tag' => 'Agua']);
                // $author->add(['tag' => 'Transito']);

                return $exp->or_([
                    $exp->and_([$author])
                ]);

            })
            ->toArray();

            }
        
        foreach ($incidents as $key => $incidence) {
            switch ($incidence['tag']) {
                case 'Transito':
                    $incidents[$key]['marker'] = $orange; 
                    break;
                case 'Parque':
                    $incidents[$key]['marker'] = $green; 
                    break;
                case 'Electrica':
                    $incidents[$key]['marker'] = $yellow;
                    break;
                case 'Agua':
                    $incidents[$key]['marker'] = $blue;
                    break;
            }
        }

        // $this->response->withType('application/json');
        // $this->response->withStringBody(json_encode($incidents));
        $this->response = $this->response->withType('application/json')->withStringBody(json_encode($incidents));
        return $this->response;
    }

    /**
     * View method
     *
     * @param string|null $id Incident id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $incident = $this->Incidents->get($id, [
            'contain' => []
        ]);

        $this->set('incident', $incident);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    // public function add()
    // {
    //     // pr("ENTRO AL ADD DEL BACK");
    //     // pr($this->request);
        
    //     if ($this->request->is('post')) {
    //         $incident = $this->Incidents->newEntity();
    //         $incident->id_user = 19;
    //         $incident->name = $this->request->getData('name');
    //         $incident->tag = $this->request->getData('tag');
    //         $incident->lat = $this->request->getData('lat');
    //         $incident->lng = $this->request->getData('lng');
    //         $incident->active = 1;

    //         if ($this->Incidents->save($incident)) {
    //             $this->response->body(json_encode($incident));
    //         } else {
    //             $this->response->body(json_encode('error'));
    //             $this->response->statusCode(500);
    //         }
            
    //         // $incident = $this->Incidents->patchEntity($incident, $this->request->getData());
    //         // if ($this->Incidents->save($incident)) {
    //         //     $this->Flash->success(__('The incident has been saved.'));

    //         //     return $this->redirect(['action' => 'index']);
    //         // }
    //         // $this->Flash->error(__('The incident could not be saved. Please, try again.'));
    //     }
    //     // $this->set(compact('incident'));
    //     return $this->response;
    // }

    /**
     * Edit method
     *
     * @param string|null $id Incident id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $incident = $this->Incidents->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $incident = $this->Incidents->patchEntity($incident, $this->request->getData());
            if ($this->Incidents->save($incident)) {
                $this->Flash->success(__('The incident has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The incident could not be saved. Please, try again.'));
        }
        $this->set(compact('incident'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Incident id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $incident = $this->Incidents->get($id);
        if ($this->Incidents->delete($incident)) {
            $this->Flash->success(__('The incident has been deleted.'));
        } else {
            $this->Flash->error(__('The incident could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function kmeans($incidents = null){

        $incidents = $this->Incidents->find('all')
        ->where(['active' => 1])
        ->toArray();

        $samples = [];
        $grouped_center = [];

        foreach ($incidents as $key => $incidence) {
            array_push($samples, array($incidence->lng, $incidence->lat) );
        }

;
        $kmeans = new KMeans(5);
        $grouped = $kmeans->cluster($samples);

        foreach ($grouped as $key => $group) {

            // pr("----GRUPO----");
            $sum_x = 0;
            $sum_y = 0;

            foreach ($group as $key2 => $value) {
                $sum_x = $value[0] + $sum_x;
                $sum_y = $value[1] + $sum_y;
            }

            $sum_x = (($sum_x)/sizeof($grouped[$key]));
            $sum_y = (($sum_y)/sizeof($grouped[$key]));

            $grouped_center[$key] = array($sum_x, $sum_y);

            $radio = 0;
            $max_x = 0;
            $max_y = 0;

            foreach ($group as $key3 => $value) {

                $diff_x = abs( (($sum_x) - ($value[0])) );

                if($max_x < $diff_x ){
                    $max_x = $diff_x;
                }

                $diff_y = abs( (($sum_y) - ($value[1])) );
                 
                if($max_y < $diff_y ){
                    $max_y = $diff_y;
                }

            }

            $grouped_center[$key][2] = $max_x;
            $grouped_center[$key][3] = $max_y;

            if($max_x > $max_y){
                $grouped_center[$key][4] = $max_x;
            }else{
                $grouped_center[$key][4] = $max_y;
            }

        }

        //ICONCOS EN DROPBOX
        $green = 'https://www.dropbox.com/s/22qd59ll3ztq96x/marker-green.png?dl=1';
        $red = 'https://www.dropbox.com/s/4szn03oymi1fq5s/marker-red.png?dl=1';
        $blue = 'https://www.dropbox.com/s/o4int3sjwoa680c/marker-blue.png?dl=1';
        $yellow = 'https://www.dropbox.com/s/jik4mvtc3ijeogj/marker-yellow.png?dl=1';
        $orange = 'https://www.dropbox.com/s/sbawj09z5do82co/marker-orange.png?dl=1';




        $groupedColors = [$red,$blue,$green,$yellow,$orange];
        $incidentsGroupedView = [];
        $incidentsGroupedView['kmeans'] = [];

        foreach ($grouped as $x => $group) {
            foreach ($group as $key2 => $value) {
                array_push($incidentsGroupedView['kmeans'], array($value[0], $value[1], $groupedColors[$x]) );
            }
        }

        $incidentsGroupedView['groupInfo'] = $grouped_center;
      
        // $this->response->withStringBody(json_encode($incidentsGroupedView));

        $this->response = $this->response->withType('application/json')->withStringBody(json_encode($incidentsGroupedView));


        return $this->response;

    }
}
