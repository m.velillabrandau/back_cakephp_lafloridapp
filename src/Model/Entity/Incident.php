<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Incident Entity
 *
 * @property int $id
 * @property int $id_user
 * @property string $name
 * @property string $tag
 * @property float $lat
 * @property float $lng
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $active
 */
class Incident extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id_user' => true,
        'name' => true,
        'tag' => true,
        'lat' => true,
        'lng' => true,
        'created' => true,
        'modified' => true,
        'active' => true
    ];
}
