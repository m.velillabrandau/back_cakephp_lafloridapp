<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $incident
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $incident->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $incident->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="incidents form large-9 medium-8 columns content">
    <?= $this->Form->create($incident) ?>
    <fieldset>
        <legend><?= __('Edit Incident') ?></legend>
        <?php
            echo $this->Form->control('id_user');
            echo $this->Form->control('name');
            echo $this->Form->control('tag');
            echo $this->Form->control('lat');
            echo $this->Form->control('lng');
            echo $this->Form->control('active');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
