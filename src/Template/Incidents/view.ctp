<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $incident
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Incident'), ['action' => 'edit', $incident->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Incident'), ['action' => 'delete', $incident->id], ['confirm' => __('Are you sure you want to delete # {0}?', $incident->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Incidents'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Incident'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="incidents view large-9 medium-8 columns content">
    <h3><?= h($incident->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($incident->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id User') ?></th>
            <td><?= $this->Number->format($incident->id_user) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lat') ?></th>
            <td><?= $this->Number->format($incident->lat) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lng') ?></th>
            <td><?= $this->Number->format($incident->lng) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Active') ?></th>
            <td><?= $this->Number->format($incident->active) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($incident->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($incident->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Name') ?></h4>
        <?= $this->Text->autoParagraph(h($incident->name)); ?>
    </div>
    <div class="row">
        <h4><?= __('Tag') ?></h4>
        <?= $this->Text->autoParagraph(h($incident->tag)); ?>
    </div>
</div>
