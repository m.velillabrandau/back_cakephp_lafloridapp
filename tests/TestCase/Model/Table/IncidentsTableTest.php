<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IncidentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IncidentsTable Test Case
 */
class IncidentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\IncidentsTable
     */
    public $Incidents;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Incidents'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Incidents') ? [] : ['className' => IncidentsTable::class];
        $this->Incidents = TableRegistry::getTableLocator()->get('Incidents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Incidents);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
